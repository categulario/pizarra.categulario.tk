use std::panic;

use wasm_bindgen::prelude::*;
use web_sys::CanvasRenderingContext2d;

use pizarra::{App, app::{MouseButton, ShouldRedraw}, point::Vec2DScreen};

mod graphics;
mod queue;

use graphics::Drawable;
use queue::{Queue, Redraw};

fn buttom_from_js(code: i32) -> MouseButton {
    match code {
        0 => MouseButton::Left,
        1 => MouseButton::Middle,
        2 => MouseButton::Right,
        _ => MouseButton::Unknown,
    }
}

#[wasm_bindgen]
pub struct Pizarra {
    app: App,
    redraw_queue: Queue,
}

#[wasm_bindgen]
impl Pizarra {
    pub fn new(width: i32, height: i32) -> Pizarra {
        Pizarra {
            app: App::new(Vec2DScreen::new(width as f64, height as f64)),
            redraw_queue: Queue::new(),
        }
    }

    pub fn width(&self) -> i32 {
        self.app.get_dimensions().x as i32
    }

    pub fn height(&self) -> i32 {
        self.app.get_dimensions().y as i32
    }

    pub fn handle_mouse_button_pressed(&mut self, button: i32, x: f64, y: f64) {
        self.redraw_queue.push(self.app.handle_mouse_button_pressed(buttom_from_js(button), Vec2DScreen::new(x, y)));
    }

    pub fn handle_mouse_button_released(&mut self, button: i32, x: f64, y: f64) {
        self.redraw_queue.push(self.app.handle_mouse_button_released(buttom_from_js(button), Vec2DScreen::new(x, y)));
    }

    pub fn handle_mouse_move(&mut self, x: f64, y: f64) {
        self.redraw_queue.push(self.app.handle_mouse_move(Vec2DScreen::new(x, y)));
    }

    pub fn set_color(&mut self, color: String) {
        if let Ok(color) = color.parse() {
            self.app.set_color(color);
        }
    }

    pub fn set_tool(&mut self, tool: String) {
        if let Ok(tool) = tool.parse() {
            self.app.set_tool(tool);
        }
    }

    pub fn set_thickness(&mut self, stroke: f64) {
        self.app.set_stroke(stroke);
    }

    pub fn set_alpha(&mut self, alpha: f64) {
        self.app.set_alpha(alpha);
    }

    pub fn zoom_in(&mut self) {
        self.app.zoom_in();
        self.redraw_queue.push(ShouldRedraw::All);
    }

    pub fn zoom_out(&mut self) {
        self.app.zoom_out();
        self.redraw_queue.push(ShouldRedraw::All);
    }

    pub fn home(&mut self) {
        self.app.go_home();
        self.redraw_queue.push(ShouldRedraw::All);
    }

    pub fn resize(&mut self, x: i32, y: i32) {
        self.app.resize(Vec2DScreen::new(x as f64, y as f64));
        self.redraw_queue.push(ShouldRedraw::All);
    }

    pub fn undo(&mut self) {
        self.app.undo();
        self.redraw_queue.push(ShouldRedraw::All);
    }

    pub fn redo(&mut self) {
        self.app.redo();
        self.redraw_queue.push(ShouldRedraw::All);
    }

    pub fn to_svg(&self) -> Option<String> {
        self.app.to_svg()
    }

    pub fn render_base(&self, ctx: &CanvasRenderingContext2d) {
        let paint_size = self.app.get_dimensions();
        ctx.set_fill_style(&JsValue::from_str(&self.app.bgcolor().css()));
        ctx.fill_rect(0.0, 0.0, paint_size.x, paint_size.y);
    }

    pub fn render_persistent(&self, ctx: &CanvasRenderingContext2d) {
        let t = self.app.get_transform();

        for cmd in self.app.draw_commands_for_drawing() {
            cmd.draw(ctx, t);
        }
    }

    pub fn render_last_shape(&self, ctx: &CanvasRenderingContext2d) {
        let t = self.app.get_transform();

        if let Some(cmd) = self.app.draw_commands_for_current_shape() {
            cmd.draw(ctx, t);
        }
    }

    pub fn queue_shape_redraw(&mut self) {
        self.redraw_queue.push(ShouldRedraw::Shape);
    }

    pub fn queue_board_redraw(&mut self) {
        self.redraw_queue.push(ShouldRedraw::All);
    }

    pub fn pop_redraw(&mut self) -> Redraw {
        self.redraw_queue.pop()
    }
}

#[wasm_bindgen]
pub fn setup() -> Result<(), JsValue> {
    panic::set_hook(Box::new(console_error_panic_hook::hook));

    Ok(())
}

# Pizarra website

Sitio web de la pizarra, así como demo en webassembly.

En este repositorio vive una versión altamente incompleta de la pizarra para el
navegador. No la hice para usarse sino para jugar con webassembly y tener un
demo web, pero no representa mucho más que eso. La pizarra es una aplicación de
escritorio.

Se puede ver el resultado aquí:

https://pizarra.categulario.xyz

## Desarrollo y construcción

Instalar las dependencias

    pipenv install

Y correr el sitio de desarrollo

    pipenv run python src/main.py --dev

o compilar el sitio para producción

    pipenv run python src/main.py

## Demo en webassembly

Necesitas [`wasm-pack`](https://rustwasm.github.io/wasm-pack/installer/) para
compilar este proyecto, si ya lo tienes instalado puedes usar:

    wasm-pack build --target web

para compilar el proyecto. Si corres un servidor local con raíz en el
repositorio podrás probar el resultado.
